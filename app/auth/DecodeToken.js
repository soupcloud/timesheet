import * as Cookies from 'js-cookie';

export const DecodeToken = () => {
  const token = Cookies.get('token');

  return token;
};
