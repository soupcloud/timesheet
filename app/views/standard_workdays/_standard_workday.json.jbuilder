json.extract! standard_workday, :id, :created_at, :updated_at
json.url standard_workday_url(standard_workday, format: :json)
