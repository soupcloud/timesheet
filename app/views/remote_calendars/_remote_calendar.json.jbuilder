json.extract! remote_calendar, :id, :name, :identity_id, :company_id, :remote_system_id, :created_at, :updated_at
json.url remote_calendar_url(remote_calendar, format: :json)
