# frozen_string_literal: true

class ChatroomsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_chatroom, only: %i[show edit update destroy]

  def index
    @chatrooms = Chatroom.all
  end

  def new
    @chatroom = Chatroom.new
  end

  def show
    messages = Message.where(chatroom: @chatroom).last(20)
    @messages = messages.map { | message |
      MessagesSerializer.new(message)
    }
    @chatroom_user =  UsersSerializer.new(current_user)
  end

  def create
    @chatroom = current_user.chatrooms.build(chatroom_params)
    @chatroom_user = @chatroom.users << current_user
    chatroom_users_params.each {|user_id|  @chatroom.users << @company.users.find(user_id) }
    @chatroom.users = @chatroom.users.uniq

    # if @chatroom.save
    #   flash[:success] = 'Chat room added!'
    #   redirect_to chatroom_path(@chatroom)
    # else
    #   render :new
    # end

    respond_to do |format|
      if @chatroom.save
        format.html { redirect_to @chatroom }
      else
        format.html { render :new}
      end
    end
  end

  def destroy
    @chatroom.messages.delete_all
    @chatroom.chatroom_users.delete_all
    @chatroom.destroy
    redirect_to chatrooms_path
  end

  private

  def set_chatroom
    @chatroom = current_user.chatrooms.find(params[:id])
  end

  def chatroom_params
    params.require(:chatroom).permit(:title)
  end

  def chatroom_users_params
    params.require(:chatroom).permit(user_id: [])
  end
end
