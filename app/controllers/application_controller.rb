# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found

  require 'company_info'
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_company, except: :login
  before_action :set_time_zone
  include Devise::Controllers::Helpers
  before_action :get_messages_notifications



  # rescue_from CanCan::AccessDenied, with: :user_not_authorized
  rescue_from CanCan::AccessDenied do |exception|
    render file: "#{Rails.root}/public/401.html", status: 401
  end

  private

  def get_messages_notifications
    # @message_notifications = MessageNotification.where(user: current_user, read_at: nil).group_by(&:chatroom)
    # @notifications = []
    # @message_notifications.each do |chatroom,messages|
      @notifications = UserNotificationService.new(@current_user.id) if @current_user.present?


  end

  def handle_not_found
    redirect_to root_path, notice: "Record not found"
  end

  # def user_not_authorized
  #   flash[:alert] = 'You are not authorized to perform this action.'
  #   redirect_to user_session_path
  # end

  def set_time_zone
    Time.zone = 'Australia/Brisbane'
  end

  # def after_sign_in_path_for(resource)
  #   stored_location_for(resource) || welcome_path
  # end
  # def current_user
  #   @current_user ||= super && User.includes(:time_sheets, :projects, :roles).find(@current_user.id)
  # end
  protected

  def set_company
    CompanyInfo.current_company = current_user&.company_id
    @company ||=  current_user&.company
  end

  # def authenticate_user!
  #
  #   # redirect_to user_session_path, :notice => 'if you want to add a notice'
  #   if you want render 404 page
  #   render :file => File.join(Rails.root, 'public/404'), :formats => [:html], :status => 404, :layout => false
  #
  # end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:password, :password_confirmation, :current_password, :email, :username, :first_name, :last_name, :street_no, :street, :city, :state,
                                                       :country, :post_code, :abn, :project_search, pay_rates_attributes: %i[id rate project_id _destroy]])
    # devise_parameter_sanitizer.permit(:account_update, keys: [:username])
  end
end
