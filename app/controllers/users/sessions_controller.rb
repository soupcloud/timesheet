class Users::SessionsController < Devise::SessionsController
  require 'company_info'
  layout 'landing_page'
  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    yield resource if block_given?
    respond_with(resource, serialize_options(resource))
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message!(:notice, :signed_in)
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end

  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message! :notice, :signed_out if signed_out
    yield if block_given?
    respond_to_on_destroy
  end

  def auth_options
    { scope: resource_name, recall: "#{controller_path}#new", company_id: current_user&.company_id}
  end
  protected

  def set_company
    CompanyInfo.current_company = current_user&.company_id
    @company =  current_user&.company
  end

end
