class MessageApi::ChatroomsController < MessageApi::MessagesController

  def get_chatrooms
    render json: @current_user.chatrooms, each_serializer: ChatroomsSerializer
  end
end
