class MessageApi::MessagesController < ActionController::Base
  require 'company_info'
  before_action :authenticate, except: :login
  before_action :set_company, except: :login
  protect_from_forgery with: :null_session
  before_action :set_chatroom,except: [:login, :get_chatrooms]



  def login
    @user = User.find_by(email: user_params[:email])
    if @user && @user.valid_password?(user_params[:password])
      @current_user = @user
      @company = @user&.company
      CompanyInfo.current_company =  @company.id
      render json: @user, serializer: UsersSerializer, status: :ok
    else
      render json: {id: '',token: '',error: 'Incorrect login'}, status: '401'
    end
  end

  def create
    message = @chatroom.messages.new(message_params)
    message.user = authenticate
    message.save!

  end

  def show

    @messages = @chatroom.messages.last(20)
    # @messages = messages.map {|m| {message: m.body, id: m.id}}
    render json:  @messages, each_serializer: MessagesSerializer, status: :ok
    end

  private

  def current_user
    @current_user ||= authenticate
  end

  def authenticate
    user =  authenticate_or_request_with_http_token do |token, options|
      # Compare the tokens in a time-constant manner, to mitigate
      # timing attacks.
      # ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)
      User.find_by(authentication_token: token)
    end
  end

  def set_chatroom
    @chatroom = Chatroom.find(params[:chatroom_id])
  end

  def message_params
    params.require(:message).permit(:body)
  end
  def user_params
    params.require(:user).permit(:email, :password)
  end

  def set_token
    payload = {name: "sophie"}
    secret_key = "masd87d9s0w02@@2348$asldfja"
    algorithm = "HS256"
    cookies[:token] = JWT.encode(payload, secret_key, algorithm)
  end

  def set_company
      @company = current_user&.company
      CompanyInfo.current_company =  @company.id
  end
end
