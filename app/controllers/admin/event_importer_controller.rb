class Admin::EventImporterController < AdminController
  require "google_calendar"
  layout 'admin'
  require "enter_time_sheets"
  def index
    project_id = params[:project_id].present? ? params[:project_id] : @current_user&.time_sheets&.last&.project_id
    @events = EventImporter.where(project_id: project_id, user: @current_user)
    respond_to do |format|
      format.html
      format.js { get_events }
    end
  end

  def get_events
    @events = []
    identity = Identity.find(params[:identity_id])
    project_id = params[:project_id]
    EventImporter.where(user: @current_user, project_id: project_id).destroy_all
    time = DateTime.current
    cal = GoogleCalendar.new(token: identity.token)
    user_id = @current_user.id
    events = cal.list_event_instances(identity.remote_calendar.remote_system_id,
      time - 12.weeks,
      time.end_of_month,
      ActiveSupport::TimeZone[time.utc_offset].tzinfo.name)

    EventImporter.transaction do
      events.each do |event|
        begin
          attrs = event.merge!(user_id: user_id, project_id: project_id)
        ei = EventImporter.new(attrs)
        ei.data = event
        ei = EnterTimeSheets.new(ei).process
        ei.save!
        @events << ei
        rescue
          event
        end
      end
    end
  end
end
