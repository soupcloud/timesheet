class Admin::InvoiceConfigurationsController < AdminController
  before_action :set_invoice_configuration, only: %i[ show edit update destroy ]

  # GET /invoice_configurations or /invoice_configurations.json
  def index
    @invoice_configurations = InvoiceConfiguration.all
  end

  # GET /invoice_configurations/1 or /invoice_configurations/1.json
  def show
  end

  # GET /invoice_configurations/new
  def new
    @invoice_configuration = InvoiceConfiguration.new
  end

  # GET /invoice_configurations/1/edit
  def edit
  end

  # POST /invoice_configurations or /invoice_configurations.json
  def create
    @invoice_configuration = InvoiceConfiguration.new(invoice_configuration_params)

    respond_to do |format|
      if @invoice_configuration.save
        format.html { redirect_to [:admin,:invoice_configurations], notice: "Invoice configuation was successfully created." }
        format.json { render :show, status: :created, location: @invoice_configuration }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invoice_configuration.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoice_configurations/1 or /invoice_configurations/1.json
  def update
    respond_to do |format|
      if @invoice_configuration.update(invoice_configuration_params)
        format.html { redirect_to  [:admin,:invoice_configurations], notice: "Invoice configuation was successfully updated." }
        format.json { render :show, status: :ok, location: @invoice_configuration }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @invoice_configuration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoice_configurations/1 or /invoice_configurations/1.json
  def destroy
    @invoice_configuration.destroy
    respond_to do |format|
      format.html { redirect_to admin_invoice_configurations_url, notice: "Invoice configuation was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_configuration
      @invoice_configuration = InvoiceConfiguration.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def invoice_configuration_params
      params.require(:invoice_configuration).permit( :project_id, :standard_period, :disclaimer, :start_date,:tax_rate, :tax_rate_label)
    end
end
