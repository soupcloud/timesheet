class Admin::DashboardController < AdminController
  layout 'admin'
  def index
    times_for_current_week
    all_by_week
  end

  def times_for_current_week
    @grouped_by_user = TimeSheet.where(time_period: Date.current.beginning_of_week..Date.current.end_of_week)
                                .group_by(&:user).map { |user, time_sheets| {user: user.email, hours: (time_sheets.pluck(:hour).sum || 0) / 60 / 60} } rescue []
  end

  def all_by_week
    @hours_hash = []
    start = DateTime.current.end_of_week - 22.week + 1.day
    finish = DateTime.current.beginning_of_week
    User.includes(:time_sheets).where(company_id: @company.id).each do |user|
      hours_array = TimeSheet.where(user: user, time_period: start..finish).group_by_minute(n: 20160) { |t| t.time_period }.map { |date, timesheet| [date, (timesheet.pluck(:hour).sum || 0) / 60 / 60] }
      hours_array.pop
      @hours_hash << {name: user.email, data: hours_array}
      # TimeSheet.all.group_by(&:user).map { |user, timesheets| {user: user.email,timesheets: timesheets.group_by_week {|t| t.time_period}.inject(0){|sum,e| sum + e.hour } }}
    end

    @hours_hash
  end
end
