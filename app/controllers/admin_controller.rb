class AdminController < ApplicationController
  layout 'admin'
  before_action :is_admin

  private

  def is_admin
    unless current_user.has_role? :admin
      redirect_to root_path, flash: 'Not Authorized'
    end
  end

end
