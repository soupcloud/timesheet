class DaysController < ApplicationController

  # GET /days
  # GET /days.json
  def index
    @project = @current_user&.projects&.first
    @project = Project.find params[:project_id] if params[:project_id].present?
    @time_sheet = TimeSheet::WorkDay.find_or_initialize_by(time_period: params[:time_period], user: current_user, project_id: params[:project_id])


    @time_sheet.description = @time_sheet.description.present?  ? @time_sheet.description : "#{@time_sheet&.user&.first_name} Hour's"

  end

  def set_standard_work_hours
    @time_period = params[:time_period]
    @start_time  = @current_user.standard_workday&.start_time
    @finish_time = @current_user.standard_workday&.finish_time
    respond_to do |format|
      format.js
    end

  end

end
