class StandardWorkdaysController < ApplicationController
  before_action :set_standard_workday, only: %i[show  update destroy]
  before_action :authenticate_user!
  load_and_authorize_resource
  # GET /standard_workdays or /standard_workdays.json
  def index
    @standard_workdays = StandardWorkday.where(user: @current_user)
  end

  # GET /standard_workdays/1 or /standard_workdays/1.json
  def show
  end

  # GET /standard_workdays/new
  def new
    @standard_workday = StandardWorkday.new
    respond_to do |format|
      format.js
    end
  end

  # GET /standard_workdays/1/edit
  def edit
    @standard_workday = StandardWorkday.find_or_initialize_by(user: @current_user)
    @time_period = params[:time_period]
    @start_time  = @current_user.standard_workday&.start_time
    @finish_time = @current_user.standard_workday&.finish_time
    respond_to do |format|
      format.js
    end
  end

  # POST /standard_workdays or /standard_workdays.json
  def create
    @standard_workday = StandardWorkday.new(standard_workday_params)
    @standard_workday.user = @current_user

    respond_to do |format|
      if @standard_workday.save
        format.html { redirect_to current_user_path, notice: "Standard workday was successfully created." }
        format.js
      else
        format.html { render :new, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /standard_workdays/1 or /standard_workdays/1.json
  def update
    respond_to do |format|
      if @standard_workday.update(standard_workday_params.merge(user_id: @current_user.id))
        format.html { redirect_to current_user_path}
        format.js
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /standard_workdays/1 or /standard_workdays/1.json
  def destroy
    @standard_workday.destroy
    respond_to do |format|
      format.html { redirect_to standard_workdays_url, notice: "Standard workday was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_standard_workday
    @standard_workday = StandardWorkday.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def standard_workday_params
    params.require(:standard_workday).permit(:start_time, :finish_time)
  end
end
