class RemoteCalendarsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_remote_calendar, only: %i[ show edit update destroy ]
  require 'google_calendar'
  # GET /remote_calendars or /remote_calendars.json
  def index
    identity = @current_user.identities.find( params[:identity_id])
    @calendars = GoogleCalendar.new(token: identity.token).list_calendars
    @remote_calendars = RemoteCalendar.where(identity: identity)
    respond_to do |format|
      format.html
      format.json { render json: {calendars: @calendars&.map { |id, name| { id: id, name: name } } }}
    end
  end

  # GET /remote_calendars/1 or /remote_calendars/1.json
  def show
  end

  # GET /remote_calendars/new
  def new
    @remote_calendar = RemoteCalendar.new
  end

  # GET /remote_calendars/1/edit
  def edit
  end

  # POST /remote_calendars or /remote_calendars.json
  def create
    @remote_calendar = RemoteCalendar.find_or_initialize_by(remote_calendar_params)

    respond_to do |format|
      if @remote_calendar.save
        format.html { redirect_to @remote_calendar, notice: "Remote calendar was successfully created." }
        format.js { redirect_to @remote_calendar, notice: "Remote calendar was successfully created." }
        format.json { render :show, status: :created, location: @remote_calendar }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @remote_calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /remote_calendars/1 or /remote_calendars/1.json
  def update
    respond_to do |format|
      if @remote_calendar.update(remote_calendar_params)
        format.html { redirect_to @remote_calendar, notice: "Remote calendar was successfully updated." }
        format.json { render :show, status: :ok, location: @remote_calendar }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @remote_calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /remote_calendars/1 or /remote_calendars/1.json
  def destroy
    @remote_calendar.destroy
    respond_to do |format|
      format.html { redirect_to remote_calendars_url, notice: "Remote calendar was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_remote_calendar
    @remote_calendar = RemoteCalendar.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def remote_calendar_params
    params.require(:remote_calendar).permit(:name, :identity_id, :company_id, :remote_system_id)
  end
end
