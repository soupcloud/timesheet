# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :set_project, only: %i[show edit update destroy hours_by_day hours_by_date_range hours_by_user time_sheets_for_week]
  before_action :authenticate_user!
  load_and_authorize_resource
  require 'github'

  def index
    @projects = Project.all.paginate(page: params[:page], per_page: 10).reverse_order
    @project = Project.new

    # auth = current_user.identities.find_by(provider: 'github').token rescue nil
    # repos = repos(auth)
    # @repo_list = repos.map do |r|
    #   [r['name'], r['id']]
    # end

  end

  # GET /projects/1
  def show
    @start_date = params[:start_date] || Date.current.beginning_of_month
    @end_date = params[:end_date] || Date.current.end_of_month
    @time = TimeSheet::WorkDay.where(time_period: @start_date..@end_date, project: @project).order(:time_period)
    @time = @time.where(user_id: params[:user_id]) if params[:user_id].present?

    respond_to do |format|
      format.html {
        @column_chart = @time.group(:time_period).sum(' finish_time - start_time ')

        @users = @project.company.users
        @chat = @project.hours_by_day
        @time = @time.paginate(page: params[:page], per_page: 20)
      }
      format.csv { send_data @time.to_csv }
      format.xlsx{
        @grouped_by_user = @time.group_by &:user
      }
      format.pdf do
        render pdf: 'Invoice', header: { right: '[page] of [topage]' }
      end
      format.js {
        @column_chart = @project.hours_by_day
        @time = @time.paginate(page: params[:page], per_page: 20)
      }
    end
  end

  # GET /projects/new
  def new
    @project = Project.new
    @project.content = "<h1>Customise Your Project's Homepage</h1><br>This is a good place for your projects description"
  end

  # GET /projects/1/edit
  def edit
    auth = current_user.identities.find_by(provider: 'github').token rescue nil
    repos = repos(auth)
    @repo_list = repos.map { |r| [r['full_name'], r['id']] }
  end

  # POST /projects
  def create
    @project = Project.new(project_params)

    @project.user = current_user
    respond_to do |format|
      if @project.save
        format.html { redirect_to projects_path, notice: 'Project was successfully created.' }
        format.js
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /projects/1
  def update
    respond_to do |format|
      # authorize @project
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /projects/1
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
    end
  end

  def hours_by_day
    @start_date = params[:start_date] || Date.current.beginning_of_month
    @end_date = params[:end_date] || Date.current.end_of_month
    render json: @project.hours_by_date_range(@start_date, @end_date)
  end

  def hours_by_user
    render json: @project.hours_by_user_by_project
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def project_params
    params.require(:project).permit(:name, :start_date, :end_date, :customer_id,:content)
  end

  # def user_not_authorized(exception)
  #   policy_name = exception.policy.class.to_s.underscore
  #
  #   flash[:error] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
  #   redirect_to(request.referrer || root_path)
  # end

  def repos(auth)
    repo_list = ['error']
    url = URI.parse("https://api.github.com/user/repos")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    req = Net::HTTP::Get.new(url.request_uri)
    req["Accept"] = 'application/vnd.github.cloak-preview'
    req["Authorization"] = "token #{auth}" if auth.present?
    res = http.request(req)
    repo_list = JSON.parse(res.body) if res.message == "OK"
    repo_list
  end

end
