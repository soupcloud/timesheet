# frozen_string_literal: true

class MessageBroadcastJob < ApplicationJob
  queue_as :default
  discard_on StandardError
  def perform(message_id)
    message = Message.unscoped.includes(:user).find(message_id)
    message_serializer = MessagesSerializer.new(message)
    ActionCable.server.broadcast("chatrooms:#{message.chatroom_id}",
                                 message_serializer.as_json
                                 )
  end
end
