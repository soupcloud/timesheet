class ChatNotificationJob < ApplicationJob
  queue_as :default

  def perform(params)
    Thread.current[:company_id] = params[:company_id]
    @notifications = UserNotificationService.new(params[:user_id])
    # Do something later
    # binding.pry
    ActionCable.server.broadcast("user_id:#{params[:user_id]}",  @notifications)
  end
end
