# frozen_string_literal: true

class Invoice < Record
  include Defaults
  resourcify
  include Calculate
  belongs_to :user
  has_many :time_sheets, inverse_of: :invoice
  accepts_nested_attributes_for :time_sheets
  belongs_to :project, dependent: :destroy
  validates_presence_of :time_sheets

  def save_invoice(project_id)
    time_sheets = TimeSheet.uninvoiced_work(project_id)
    project = Project.find(project_id)
    customer = project.customer
    owner = project.user
    company = project.company
    invoice_configuration = company.invoice_configuration
    invoice = Invoice.new(user_id: owner.id,
                          project_id: project.id,
                          owner_first_name: owner.first_name,
                          owner_last_name: owner.last_name,
                          owner_email: owner.email,
                          owner_street_no: company.street_no,
                          owner_street: company.street,
                          owner_city: company.city,
                          owner_state: company.state,
                          owner_country: company.country,
                          owner_post_code: company.post_code,
                          owner_abn: company.abn,
                          customer_first_name: customer.first_name,
                          customer_last_name: customer.last_name,
                          customer_street_no: customer.street_no,
                          customer_street: customer.street,
                          customer_city: customer.city,
                          customer_state: customer.state,
                          customer_country: customer.country,
                          customer_post_code: customer.post_code,
                          customer_abn: customer.abn,
                          invoice_date: Date.today,
                          tax_rate: invoice_configuration&.tax_rate,
                          tax_rate_label: invoice_configuration&.tax_rate_label,
                          disclaimer: invoice_configuration&.disclaimer)
    invoice.time_sheets << time_sheets

    invoice
  end

  def owner_full_name
    first = owner_first_name || "No First Name"

    last = owner_last_name || "No Last Name"
    first + " " + last
  end

  def customer_full_name
    first = customer_first_name || "No First Name"

    last = customer_last_name || "No Last Name"
    first + " " + last
  end

  def owner_address
    address = "#{owner_street_no}   #{owner_street}<br>#{owner_city}  #{owner_state}  <br>	#{owner_country} #{owner_post_code}".html_safe
  end

  def customer_address
    address = "#{customer_street_no}   #{customer_street}<br>#{customer_city}  #{customer_state}  <br>	#{customer_country} #{customer_post_code}".html_safe
  end

  def total_for_user
    total = []
    time_sheets = self.time_sheets.includes(:user)
    time_sheets.each do |timesheet|
      total << (timesheet.user.rate(timesheet.date) * timesheet.hour).to_i
    end
    total
  end

  def invoice_total(row_totals)
    row_totals.inject(0) { |sum, x| sum + x }
  end
end
