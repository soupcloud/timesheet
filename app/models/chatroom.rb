# frozen_string_literal: true

class Chatroom < Record
  include Defaults
  has_many :messages, dependent: :destroy
  has_many :chatroom_users, dependent: :destroy
  has_many :users, through: :chatroom_users , dependent: :destroy

  validate :unique_name_in_company
  validates_presence_of :title

  private

  def unique_name_in_company
    names = company.chatrooms.pluck(:title)
    if names.include? title
      errors.add(:title, 'must be unique')
    end
  end

end
