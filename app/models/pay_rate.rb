# frozen_string_literal: true

class PayRate < Record
  include Defaults
  belongs_to :project , dependent: :destroy
  belongs_to :user , dependent: :destroy
  validates_presence_of :rate, message: 'User must have pay rate'
  resourcify


end
