module Defaults
  extend ActiveSupport::Concern


  def self.included(base)
    base.class_eval do
      belongs_to :company
      before_create :set_company
      default_scope { where(company_id: Thread.current[:company_id])}
    end
  end


  def current_company
    Thread.current[:company_id]
  end

  private



  def set_company
    company_id = current_company
  end
end
