# frozen_string_literal: true

class Message < Record
  include Defaults
  belongs_to :user
  belongs_to :chatroom
  after_save :send_message
  validates :body, presence: true, length: {minimum: 1, maximum: 1000}

  def timestamp
    created_at.strftime("%H:%M:%S %d %B %Y")
  end

  private

  def send_message
    MessageBroadcastJob.perform_now(id)
    users = chatroom.users.where.not(id: user_id)
    users.each do |user|
      MessageNotification.create(message: self, user: user, chatroom: chatroom)
    end
  end
end
