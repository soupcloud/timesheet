class Company < ApplicationRecord
  resourcify
  validates_uniqueness_of :name, :allow_blank => true
  has_many :users
  has_many :projects
  has_many :chatrooms
  has_many :chatroom_users
  has_many :invoices
  has_many :customers
  has_many :vehicles
  has_many :identities
  has_one :invoice_configuration
  validates_format_of :email, with: URI::MailTo::EMAIL_REGEXP, :allow_blank => true


  def address
     "#{street_no}   #{street}<br>#{suburb}  #{state}  <br>	#{country} #{post_code}".html_safe
  end
end
