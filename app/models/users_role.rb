# frozen_string_literal: true

class UsersRole < Record
  include Defaults
  belongs_to :user, dependent: :destroy
  belongs_to :role, dependent: :destroy
  belongs_to :company, required: false
end
