# frozen_string_literal: true

class Identity < Record
  belongs_to :user, dependent: :destroy
  include Defaults
  validates :uid, uniqueness: { message: 'already subscribed for another account' }
  has_one :remote_calendar

  # resourcify
end
