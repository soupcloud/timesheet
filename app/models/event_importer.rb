class EventImporter < ApplicationRecord
  include Defaults
  belongs_to :project
  belongs_to :company
  belongs_to :user
  belongs_to :time_sheet, required: false
end
