# frozen_string_literal: true

require "csv"
require "google_calendar"
class TimeSheet < Record
  include Defaults
  include Calculate
  resourcify
  # validates_presence_of :time_period, :hour
  has_many :travels, dependent: :destroy
  accepts_nested_attributes_for :travels, allow_destroy: true
  # has_many :works, dependent: :destroy
  belongs_to :project
  belongs_to :invoice, required: false
  # accepts_nested_attributes_for :works, allow_destroy: true, :reject_if => lambda {|a| a[:hour].blank?}
  # validates_presence_of :time_period
  belongs_to :user
  has_many :works, dependent: :destroy
  scope :date_range, ->(start_date, end_date) { where(time_period: start_date..end_date) }
  scope :for_project, ->(project) { where(projects: project) }
  # validates_presence_of :description
  before_validation :set_hours
  def self.to_csv(options = {})
    ::CSV.generate(options) do |csv|
      csv << ["user_name", "date Worked", "hours", "rate"]
      all.each do |p|
        csv << [p.user.full_name, p.time_period, p.hour, p.user.rate(p.time_period)]
      end
    end
  end

  def has_invoice?
    invoice_id.present?
  end

  def hour
    self[:hour] / 3600 rescue 0
  end

  def self.uninvoiced_work(project_id)
    # select(:hour, :id, :time_sheet_id)
    where("invoice_id IS ?", nil)
  end

  def self.filter_buy_date(start_date, end_date)
    where("time_period \tBETWEEN ? AND ? ", start_date.to_s, end_date.to_s)
  end

  # def self.has_time_sheet(day, user)
  #   Work.joins(:time_sheet).where('time_sheets.time_period = ?', day).where('time_sheets.user_id =?', user).exists?
  # end

  # def has_time_sheet?
  #   # Work.joins(:time_sheet).where('time_sheets.time_period = ?', day).where('time_sheets.user_id =?', user).exists?
  #   self.present?
  # end

  def self.for_project(project)
     project.time_sheets
  end

  def set_hours
    # hour =  finish_time - start_time
    self.hour = if finish_time.present? && start_time.present? && finish_time > start_time
      finish_time - start_time
    else
      0
    end
  end
end
