class MessageNotification < Record
  include Defaults
  belongs_to :chatroom
  belongs_to :user
  belongs_to :message
  after_create :notify_users

  private

  def notify_users
    ChatNotificationJob.perform_later(company_id: company_id, user_id: user_id)
  end

end
