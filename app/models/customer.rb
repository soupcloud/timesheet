# frozen_string_literal: true

class Customer < Record
  include Defaults
  has_one :project, dependent: :destroy
  has_rich_text :content

  resourcify

  def full_name
    first = first_name || "No First Name"

    last = last_name || "No Last Name"
    first + " " + last
  end

  def address
     "#{street_no}   #{street}<br>#{city}  #{state}  <br>	#{country} #{post_code}".html_safe
  end
end
