class StandardWorkday < Record
  include Defaults
  resourcify
  belongs_to :user
  belongs_to :company
end
