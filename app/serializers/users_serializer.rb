class   UsersSerializer < ActiveModel::Serializer
  attributes :id, :username, :token, :error, :full_name

  def error
    ''
  end
  def token
    object.authentication_token
  end

  def full_name
    object.full_name
  end
end
