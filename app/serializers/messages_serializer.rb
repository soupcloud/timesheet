class MessagesSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :message, :chatroom_id, :username, :message_date,:avatar

  def message
    object.body
  end

  def username
    object.user.full_name
  end

  def message_date
    object.created_at.utc
  end

  def avatar
    ActionController::Base.helpers.image_url(object.user.avatar)
  end

end
