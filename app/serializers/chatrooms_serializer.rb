class ChatroomsSerializer < ActiveModel::Serializer
  attributes :id, :title
  has_many :chat_users, serializer: UsersSerializer

  def chat_users
    object.users.where.not(id: current_user.id)
  end

end
