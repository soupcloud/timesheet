class UserNotificationService

  def self.new(user_id)
    message_notifications = MessageNotification.where(user_id: user_id, read_at: nil).group_by(&:chatroom)
    notifications = []
    message_notifications.each do |chatroom,messages|
      notifications << {chatroom: chatroom.title,messages: messages.count }
    end
    notifications
  end

end
