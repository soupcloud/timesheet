import React, { useState, useEffect, useRef } from 'react';

import actioncable from 'actioncable';
import { formatTimestamp } from './lib/FormatTimestamp';

function RemoteCalendar(props) {
  const [calendar, setCalendar] = useState({id: props.default_calendar.remote_system_id,name: props.default_calendar.name});
  const [remoteCalendars, setRemoteCalendars] = useState([]);
  const [error, setError] = useState(false);

  const myRef = useRef(null);
// console.log( props.default_calendar)
// console.log( calendar)
  const executeScroll = () => {
    myRef.current?.scrollIntoView();
  };
  // console.log($('meta[name="csrf-token"]').attr('content'))

  useEffect(() => {
      fetch(  `/identities/remote_calendars.json?identity_id=${props.identity_id}`)
          .then(response => {
              if (response.ok){
                  return response.json()
              }else{
                  setError(true)
              }
          }).then((data) => {
              if(data && error === false){
                  setRemoteCalendars(data.calendars)
              }
      })
  }, []);

  const saveCalendar = (e) => {
    e.preventDefault();
    let request_type = 'POST'
    let url = 'remote_calendars'
    if (props.default_calendar.id){
      request_type = 'PATCH'
      url = `remote_calendars/${props.default_calendar.id}.json`
    }
    // const formData = new FormData()
    // formData.append('remote_calendar[identity_id]',props.identity_id)
    // formData.append('remote_calendar[name]',calendar.name)
    // formData.append('remote_calendar[remote_system_id]',calendar.id)
   let new_calendar = remoteCalendars.find((c) => c.id === calendar.id)
    const requestOptions = {
      method: request_type,
      headers: { 'Content-Type': 'application/json' ,'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
      body: JSON.stringify({ remote_calendar: { identity_id: props.identity_id, name: calendar.name, remote_system_id: calendar.id }})
    };

    fetch(url, requestOptions)
        // .then(response => response.json())
  }

  const renderCalendar = (cal) => {
     return(<option  value={cal.id}>{cal.name}</option>)
  }
  const selectCalendar = (event) => {
    let selected =  remoteCalendars.filter((cal) => cal.id === event.target.value)
      if (selected.length > 0){
          setCalendar(selected[0])
      }
  }

  const renderCalendars = () => {
      if(error){
          return <div><div>Oh No You Don't Have Permission</div>
              <select key={'calendar_select'} value={calendar.id} onChange={selectCalendar}>
                  <option value={'null'}>Blank</option>
              </select></div>
      }else{
          if (remoteCalendars.length > 0) {
              return <select key={'calendar_select'} value={calendar.id} onChange={selectCalendar}>
                      <option value={'null'}>Blank</option> {remoteCalendars.map((calendar) => renderCalendar(calendar))}
                   </select>
          }else{
              return(<div>
                  <div>No Calendars Found</div>
                      <select key={'calendar_select'} value={calendar.id} onChange={selectCalendar}>
                          <option value={'null'}>Blank</option>
                      </select>
                  </div>
              )
          }
      }

  }




  return (
    <div
        key={'calendar-form-div'}
      style={{
      }}
    >
      <form key={'calendar-form'} onSubmit={saveCalendar}>
     <div> {renderCalendars()}</div>
        <input type="submit" value="Save" className="btn btn-default"/>
      </form>
      <div ref={myRef} />
    </div>

  );
}

export default RemoteCalendar;
