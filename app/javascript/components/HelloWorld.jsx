import React, { useState, useEffect, useRef } from 'react';

import actioncable from 'actioncable';
import { formatTimestamp } from './lib/FormatTimestamp';

function HelloWorld(props) {
  const [messages, setMessages] = useState(props.messages);
  window.cable = actioncable.createConsumer(`wss://${window.location.host}/cable`);
  const myRef = useRef(null);

  const executeScroll = () => {
    myRef.current?.scrollIntoView();
  };

  useEffect(() => {
    const unsubscribe = () => {
      window.cable.subscriptions.consumer.disconnect();
    };
    executeScroll();
    window.cable.subscriptions.create({
      channel: 'ChatroomsChannel',
      chatroom_id: props.chatroom_id
    }, {
      // from Rails app app/channels/chat_channel.rb
      received: (data) => {
        if (data.chatroom_id === props.chatroom_id) {
          // console.log('yes');
        } else {
          // console.log('no');
        }
        setMessages((messages) => [...messages, data]);

        executeScroll();
      },
      connected: () => {
        // console.log('connected');
      },
    }, []);
    return unsubscribe;
  }, []);

  const renderMessages = () => (<ul>{messages.slice(-20)
    .map((message) => renderMessage(message))}</ul>);

  const renderMessage = (message) => {
    if (message.user_id === props.user_id) {
      return renderCurrentUserMessage(message);
    }
    return renderUserMessage(message);
  };

  const renderCurrentUserMessage = (message) => (
    <div
      className="message-container  "
      key={message.id.toString()}>
      <div className="  message message-current-user ">
        <div className="">
          <div className="message-top-row ">
            <div className="message-body ">
              {message.message}
            </div>
            <img src={message.avatar} alt="" height="40px" width="40px"/>
          </div>
          <div className="message-bottom-row ">
            <div className="message-username">{message.username}</div>
            <div className="message-time">
              {formatTimestamp(message.message_date)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  const renderUserMessage = (message) => (
    <div
      className="message-container  "
      key={message.id.toString()}>
      <div className="  message message-user  ">
        <div className="">
          <div className="message-top-row ">
            <img src={message.avatar} alt="" height="40px" width="40px"/>
            <div className="message-body ">
              {message.message}
            </div>

          </div>
          <div className="message-bottom-row ">
            <div className="message-username">{message.username}</div>
            <div className="message-time">
              {formatTimestamp(message.message_date)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div
      style={{
        position: 'absolute',
        right: '0px',
        left: '0px',
        top: '100px',
        bottom: '110px',
        overflowY: 'auto',
      }}
    >
      {renderMessages()}
      <div ref={myRef}/>
    </div>

  );
}

export default HelloWorld;
