import moment from 'moment-timezone';

export const formatTimestamp = (timestamp) => moment(timestamp).tz(moment.tz.guess()).format('DD/MM/YYYY, h:mm');
