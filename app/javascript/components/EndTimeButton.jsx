import React, {useEffect, useState} from "react";
import moment from "moment";

function EndTimeButton(props) {
	const [endTime, setEndTime] = useState(moment().format("h:m"));


	const handleClick = (i, key) => {
		console.log(i);
		console.log(key);
	};

	const hoursList = Array.from(Array(13).keys());
	const minutesList = Array.from(Array(12).keys()).map((i) => (i * 5));
	const amPm = ["am", "pm"];
	const selectorList = (list, key) => {
		return list.map((i) => {
			return <div key={`${key}-${i}`} onClick={() => handleClick(i, key)} className="dropdown-item">{i}</div>;
		});
	};

	const DropDown = () => {
		return <div className="time-dropdown">
			<div className="dropdown-display-row">
				<input type="text" name="start-time" placeholder="Start Time"
					className="start-time-field form-control"/>
				<div className="dropdown ">
					<button className="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select
					</button>
					<div className="dropdown-menu" aria-labelledby="dropdownMenuButton" style={{width: 50}}>
						<div className="dropdown-row">
							<div className="dropdown-hours-column">{selectorList(hoursList, "hours")}</div>
							<div className="dropdown-hours-column">{selectorList(minutesList, "minutes")}</div>
							<div className="dropdown-hours-column">{selectorList(amPm, "ampm")}</div>
						</div>
					</div>
				</div>
			</div>
		</div>;
	};


	return DropDown();
}

export default EndTimeButton;
