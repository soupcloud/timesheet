import React, { useEffect, useState } from 'react';
import moment from 'moment';

function StartTimeButton(props) {
  const [startTime, setStartTime] = useState();
  const formatDD = (dd) => `0${dd}`.slice(-2);
  // const renderMethod = !!module.hot ? ReactDOM.render : ReactDOM.hydrate

  useEffect(() => {
    const initialTime = moment(props.initial_time || props.time_period).clone();
    setStartTime((startTime) => initialTime);
  }, []);

  useEffect(() => {
    setHours();
  },[startTime])

  // useEffect(() =>{
  //     $(`#hours-column-${props.id}`).scroll( (event) => {
  //         // let scroll = event.scrollTop();
  //         console.log($(`#hours-column-${props.id}`).scrollTop())
  //     });
  //
  //     $(`#minutes-column-${props.id}`).scroll( (event) => {
  //         // let scroll = event.scrollTop();
  //         console.log($(`#minutes-column-${props.id}`).scrollTop())
  //
  //     });
  //
  //     $(`#meridiem-column-${props.id}`).scroll( (event) => {
  //         // let scroll = event.scrollTop();
  //         console.log($(`#meridiem-column-${props.id}`).scrollTop())
  //     });
  //
  //
  // })

  const hoursList = Array.from(Array(12).keys()).map((i) => formatDD(i + 1));
  const minutesList = Array.from(Array(12).keys()).map((i) => formatDD(i * 5));
  const amPm = ['AM', 'PM'];

  const handleClick = (i, key) => {
    const m = startTime.format('A');
    if (key === 'minutes') {
      setStartTime((startTime) => startTime.clone().minutes(i));
    }
    if (key === 'hours') {
      if (m === 'PM') {
        i = formatDD(parseInt(i) + 12);
      }
      setStartTime((startTime) => startTime.clone().hours(i));
    }

    if (key === 'meridiem') {
      if (i === 'AM' && i !== m) {
        setStartTime((startTime) => startTime.clone().subtract(12, 'hours'));
      }
      if (i === 'PM' && i !== m) {
        setStartTime((startTime) => startTime.clone().add(12, 'hours'));
      }
    }
  };
  const getTimeStamp = () => {
    if (startTime) {
      return startTime.format();
    }
  };

  const getTime = () => {
    if (startTime) {
      return startTime.format('h:mm A');
    }
  };

  const selectorList = (list, key) => list.map((i) => <div key={`${key}-${i}`} onClick={() => handleClick(i, key)} className="dropdown-item">{i}</div>);
  const handleChange = () => {
    console.log('changed');
  };

  const DropDown = () => (
    <div className="time-dropdown">
      <div className="dropdown-display-row">

        <div className="dropdown" id={`timepicker-${props.name}`} data-timestamp={getTimeStamp()}>

          <input
            style={{ minWidth: 100 }}
            type="text"
            defaultValue={getTime()}
            onChange={() => handleChange()}
            className="start-time-field form-control dropdown-toggle readonlyjm"
            id={props.id}
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          />
          <input type="hidden" name={props.name} defaultValue={getTimeStamp()} id={`hidden-${props.id}`} />

          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <form action="">
              <div className="dropdown-row">

                <div id={`hours-column-${props.id}`} className="dropdown-hours-column">{selectorList(hoursList, 'hours')}</div>
                <div id={`minutes-column-${props.id}`} className="dropdown-hours-column">{selectorList(minutesList, 'minutes')}</div>
                <div id={`meridiem-column-${props.id}`} className="dropdown-hours-column">{selectorList(amPm, 'meridiem')}</div>

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );

  return DropDown();
}

export default StartTimeButton;
