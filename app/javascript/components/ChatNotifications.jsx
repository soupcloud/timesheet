import React, { useState, useEffect } from 'react';

import actioncable from 'actioncable';
import { formatTimestamp } from './lib/FormatTimestamp';

function ChatNotifications(props) {
  const [messages, setMessages] = useState(props.messages);
  const [showHide, setShowHide] = useState(true);
  window.cable1 = actioncable.createConsumer(`ws://${window.location.host}/cable`);
  useEffect(() => {
    const unsubscribe = () => {
      window.cable1.subscriptions.consumer.disconnect();
    };
    // executeScroll();
    window.cable1.subscriptions.create({
      channel: 'ChatNotificationsChannel',
      user_id: props.user_id,
    }, {
      // from Rails app app/channels/chat_channel.rb
      received: (data) => {
        console.log(data);
        setMessages(data);

        // executeScroll();
      },
      connected: () => {
        console.log('connected notify');
      },
    }, []);
    return unsubscribe;
  }, []);

  const toggleMessages = () => {
    if (showHide) {
      setShowHide(false);
    } else {
      setShowHide(true);
    }
  };

  const renderMessage = (message) => (
    <div key={message.chatroom}>
      {message.chatroom}
      {message.messages}
    </div>
  );

  const renderMessages = () => {
    const message_count_array = messages.map((m) => m.messages);
    let totalMessageCount;
    totalMessageCount = message_count_array.reduce(
      (previousMessageCount, currentMessageCount, index) => previousMessageCount + currentMessageCount,
      0,
    );

    if (messages.length > 0) {
      if (showHide) {
        return (<div className="chat-notify-dropdown">{messages.map((message) => renderMessage(message))}</div>);
      }
      return (<div style={{ border: '1px solid white' }} className="chat-notify-dropdown" onClick={() => toggleMessages()}>{totalMessageCount}</div>);
    }
  };

  return (
    <div style={{ color: 'white' }}>
      {renderMessages()}
    </div>

  );
}

export default ChatNotifications;
