// require("@rails/ujs").start()
// require("turbolinks").start()
// require("@rails/activestorage").start()
// require("channels")

// import 'bootstrap/dist/js/bootstrap';
// import("styles/application")
// import "./stylesheets/time_sheets.scss"
// import 'bootstrap/dist/js/bootstrap';

//
// document.addEventListener("turbolinks:load", () => {
//     $('[data-toggle="tooltip"]').tooltip()
//     $('[data-toggle="popover"]').popover()
//     $('.toast').toast({ autohide: false })
//     $('#toast').toast('show')
// })// Support component names relative to this directory:
// import '@fontawesome/fontawesome-free/js/all';

require('trix');
require('@rails/actiontext');

// import 'trix';
// import '@rails/actiontext';

const componentRequireContext = require.context('components', true);
const ReactRailsUJS = require('react_ujs');

ReactRailsUJS.useContext(componentRequireContext);
