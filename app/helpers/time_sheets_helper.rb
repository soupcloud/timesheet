# frozen_string_literal: true

module TimeSheetsHelper
  def my_calendar(options = {}, &block)
    raise 'month_calendar requires a block' unless block_given?
    MyCalendar.new(self, options).render(&block)
  end

  def get_duration_hrs_and_mins(duration)
    hours = duration / (3600000 * 3600000)
    minutes = (duration / 60000) % 60000
    "#{hours.round(2)}h #{minutes.round(2)}m"
  rescue
    ""
  end
end
