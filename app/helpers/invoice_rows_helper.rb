# frozen_string_literal: true

module InvoiceRowsHelper
  def seconds_to_hms(sec)
    sec.round(2)
    # "%02d:%02d:%02d" % [sec / 3600, sec / 60 % 60, sec % 60]
  end
end
