# frozen_string_literal: true
require 'cgi'
require 'active_support'
module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
      logger.add_tags 'ActionCable', current_user.email
    end

    protected

    def find_verified_user # this checks whether a user is authenticated with devise

      if env['warden'].user.nil?
      # if @current_user.nil?


        # base64_text = request.query_parameters[:token]
        # first_part_text, second_part_text = base64_text.split('--')
        #
        # token =  Rack::Session::Cookie::Base64::Marshal.new.decode(first_part_text)
        # @current_user = User.find_by(authentication_token: verify_and_decrypt_session_cookie(base64_text))
        @current_user = User.find_by(authentication_token:  request.query_parameters[:token]) if request.query_parameters[:token].present?
      else
        env['warden']&.user
      #   reject_unauthorized_connection
      end
    end



    def verify_and_decrypt_session_cookie(cookie, secret_key_base = Rails.application.secret_key_base)
      cookie = CGI::unescape(cookie)
      salt   = 'authenticated encrypted cookie'
      encrypted_cookie_cipher = 'aes-256-gcm'
      serializer = ActiveSupport::MessageEncryptor::NullSerializer

      key_generator = ActiveSupport::KeyGenerator.new(secret_key_base, iterations: 1000)
      key_len = ActiveSupport::MessageEncryptor.key_len(encrypted_cookie_cipher)
      secret = key_generator.generate_key(salt, key_len)
      encryptor = ActiveSupport::MessageEncryptor.new(secret, cipher: encrypted_cookie_cipher, serializer: serializer)

      encryptor.decrypt_and_verify(cookie)
    end
  end
end
