class ChatNotificationsChannel < ApplicationCable::Channel
  def subscribed
    # current_user.chatrooms.each do |chatroom|
    stream_from "user_id:#{params[:user_id]}"
    # end
  end

  def unsubscribed
    stop_all_streams
  end

  def send_message(data)
    # console.log(data)
    # ChatNotificaitonJob.perform_now(data)
  end
end
