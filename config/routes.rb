# frozen_string_literal: true

Rails.application.routes.draw do

  namespace :admin do
    resources :invoice_configurations
    get 'dashboard', to: 'dashboard#index'
    get 'event_importer', to: 'event_importer#index'
    resources :companies, except: :index
    resources :projects
    authenticate(:user) do
      resources :users do
        resources :projects
        resources :identities
        resources :pay_rates
        resources :pay_obligations
        get :user_data
        get :user_income
        get :user_permission
        post :user_permission
      end
    end
  end
  scope :identities do
    resources :remote_calendars
  end
  get 'edit_standard_workday', to: 'standard_workdays#edit'
  resources :standard_workdays
  get '/set_standard_work_day', to: 'set_standard_work_day#standard_workdays'
  resources :companies, except: :index
  namespace :message_api do
    get 'chatrooms', to: 'chatrooms#get_chatrooms'
    post 'login', to: 'messages#login'
    post 'chatroom/:chatroom_id', to: 'messages#create'
    get 'chatroom/:chatroom_id', to: 'messages#show'
  end
  get '/home', to: 'landing_page#index'
  get '/change_password', to: 'users#change_password'
  get 'current_user', to: 'user#edit'
  patch 'current_user', to: 'user#update'
  resources :travels
  namespace :projects do
    get 'time_sheet_for_week/show'
  end
  resources :vehicles do
    '#my-container'
    resources :travels
  end
  resources :pay_rates
  resources :identities
  resources :projects do
    resources :invoices, except: [:edit], controller: 'projects/invoices'
  end
  resources :invoices, except: [:edit]
  resources :works
  get '/send_invoice', to: 'invoices#send_invoice', as: :send_invoice
  resources :social_info
  resources :chatrooms do
    resource :chatroom_users
    resources :messages
  end
  resources :time_sheets, as: :time_sheet_work_days
  resources :time_sheets do
    resources :travels
    collection do
      post 'hours_for_day/:date', to: 'time_sheets#hours_for_day', as: '_hours'
    end
  end

  get '/current_day/:time_period', to: 'days#index', as: :current_day
  get '/set_standard_work_hours', to: 'days#set_standard_work_hours'
  resources :customers do
    get :details
  end

  resources :projects do
    collection do
      get :all_work_for_project
      get '/hours_by_day/:id/', to: 'projects#hours_by_day', as: :hours_by_day
      get '/hours_by_user_by/:id', to: 'projects#hours_by_user', as: :hours_by_user
      get '/:id/time_sheets_for_week(.:format)', to: 'projects#time_sheets_for_week', as: :time_sheets_for_week
      # resources :time_sheets_for_week
    end
  end

  mount ActionCable.server => '/cable'

  devise_for :users,
             controllers: {
               registrations: 'users/registrations',
               sessions: 'users/sessions',
               omniauth_callbacks: 'users/omniauth_callbacks'
               # passwords: 'users/passwords'
             }
  get 'auth/:provider/callback', to: 'time_sheet#index'

  root to: 'time_sheets#index'
  # root to: "landing_page#index"
end
