# frozen_string_literal: true

require 'test_helper'
require 'google_calendar'

class GoogleCalendarTest < ActiveSupport::TestCase
  setup do
    @auth ='ya29.a0AfH6SMAIrKqqBBuP3lgp4jSw5L0d3uItarfutT_5LDVVPXxL3bZC4qaKvpyj1ZMXAl4_cbBhA6gQRmbHg-MmPYiXYHXeq7xRcTcAe5hdUUJc9G4HCifYmc3QZoHwm-D3rv5fCcn5FgUQmrsEY1owqir2qJokIw'
    @cal = GoogleCalendar.new(token: @auth)
  end

  test 'get list of calendars' do

    list = @cal.list_calendars
    assert list.present?
    ap list
  end

  test 'get list of events' do
    time = DateTime.current

    events = @cal.list_events('chris78323@gmail.com', time.beginning_of_day, time.end_of_day)
    # assert_equal(events, [['2h4bgtku9s73nml33t9revljql', "Chris hour's"]])
    assert events.present?
  end

  test 'get event' do

    event = @cal.get_event('chris78323@gmail.com', '2h4bgtku9s73nml33t9revljql')
    assert event.present?
  end

  test 'insert event' do
    timestamp = DateTime.current
    start_time = timestamp.change({ hour: 7, min: 0 })
    end_time = timestamp.change({ hour: 15, min: 0 })

    event = @cal.update_or_create_event(calendar_id: 'chris78323@gmail.com', attendee: 'chris78323@gmail.com', summary: "Chris Hosadfdfur's", start_time: start_time, end_time: end_time, timezone: ActiveSupport::TimeZone[timestamp.utc_offset].tzinfo.name)
    assert event.present?
  end

  test 'get list of events month' do
    time = DateTime.current

    events = @cal.list_event_instances('c_pl6ed1gplo2p6874kq3jpqav7o@group.calendar.google.com', time.beginning_of_month, time.end_of_month, ActiveSupport::TimeZone[time.utc_offset].tzinfo.name)
    # assert_equal(events, [['2h4bgtku9s73nml33t9revljql', "Chris hour's"]])
    assert events.present?
    CSV.open("work_for_month.csv", "wb") do |csv|
      csv << events.first.keys # adds the attributes name on the first line
      events.each do |hash|
        csv << hash.values
      end
    end
  end

  test 'get public holidays' do
    time = DateTime.current
    pb = @cal.full_list_event_instances("en.australian#holiday@group.v.calendar.google.com",
                                   time.beginning_of_year,
                                   time.end_of_month,
                                   ActiveSupport::TimeZone[time.utc_offset].tzinfo.name)
    pb
  end

end
