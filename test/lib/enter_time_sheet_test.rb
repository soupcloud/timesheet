# frozen_string_literal: true

require "test_helper"
require "csv"
require "enter_time_sheets"
class EnterTimeSheetsTest < ActiveSupport::TestCase
  # setup do
  #   @work_file = CSV.read('test/lib/work_for_month.csv')
  # end

  test "parse work file " do
    # ap @work_file
    events = []
    company = companies(:one)
    project = projects(:one)
    CSV.foreach("test/lib/work_for_month.csv", headers: true) do |row|
      events << row.to_hash
    end
    EnterTimeSheets.new(events, company,project).process

    time_sheets = TimeSheet.all
    time_sheets
    assert_equal 30, time_sheets.count
  end
end
