require "application_system_test_case"

class RemoteCalendarsTest < ApplicationSystemTestCase
  setup do
    @remote_calendar = remote_calendars(:one)
  end

  test "visiting the index" do
    visit remote_calendars_url
    assert_selector "h1", text: "Remote Calendars"
  end

  test "creating a Remote calendar" do
    visit remote_calendars_url
    click_on "New Remote Calendar"

    fill_in "Company", with: @remote_calendar.company_id
    fill_in "Identity", with: @remote_calendar.identity_id
    fill_in "Name", with: @remote_calendar.name
    fill_in "Remote system", with: @remote_calendar.remote_system_id
    click_on "Create Remote calendar"

    assert_text "Remote calendar was successfully created"
    click_on "Back"
  end

  test "updating a Remote calendar" do
    visit remote_calendars_url
    click_on "Edit", match: :first

    fill_in "Company", with: @remote_calendar.company_id
    fill_in "Identity", with: @remote_calendar.identity_id
    fill_in "Name", with: @remote_calendar.name
    fill_in "Remote system", with: @remote_calendar.remote_system_id
    click_on "Update Remote calendar"

    assert_text "Remote calendar was successfully updated"
    click_on "Back"
  end

  test "destroying a Remote calendar" do
    visit remote_calendars_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Remote calendar was successfully destroyed"
  end
end
