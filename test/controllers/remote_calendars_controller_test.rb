require 'test_helper'

class RemoteCalendarsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @remote_calendar = remote_calendars(:one)
  end

  test "should get index" do
    @identity = identities(:one)
    get remote_calendars_url, params:{identity_id: @identity.id}, headers: { "Content-Type": "application/json" }
    assert_response :success
  end

  test "should get new" do
    get new_remote_calendar_url
    assert_response :success
  end

  test "should create remote_calendar" do
    assert_difference('RemoteCalendar.count') do
      post remote_calendars_url, params: { remote_calendar: { company_id: @remote_calendar.company_id, identity_id: @remote_calendar.identity_id, name: @remote_calendar.name, remote_system_id: @remote_calendar.remote_system_id } }
    end

    assert_redirected_to remote_calendar_url(RemoteCalendar.last)
  end

  test "should show remote_calendar" do
    get remote_calendar_url(@remote_calendar)
    assert_response :success
  end

  test "should get edit" do
    get edit_remote_calendar_url(@remote_calendar)
    assert_response :success
  end

  test "should update remote_calendar" do
    patch remote_calendar_url(@remote_calendar), params: { remote_calendar: { company_id: @remote_calendar.company_id, identity_id: @remote_calendar.identity_id, name: @remote_calendar.name, remote_system_id: @remote_calendar.remote_system_id } }
    assert_redirected_to remote_calendar_url(@remote_calendar)
  end

  test "should destroy remote_calendar" do
    assert_difference('RemoteCalendar.count', -1) do
      delete remote_calendar_url(@remote_calendar)
    end

    assert_redirected_to remote_calendars_url
  end
end
