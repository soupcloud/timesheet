import React from 'react';
// import { isTSAnyKeyword } from '@bable/types';
import actioncable from 'actioncable';
import { render, screen } from '@testing-library/react';
import ChatNotifications from '../../app/javascript/components/ChatNotifications';

it('renders without crashing', () => {
  render(<ChatNotifications messages={[]} />);
});
