# frozen_string_literal: true

require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase
  # include Devise::Test::IntegrationHelpers
  # include Warden::Test::Helpers
  def setup
    @invoice = invoices(:one)
  end

  test 'save invoice' do
    project = projects(:one)
    assert @invoice.save_invoice(project.id)
  end

  test 'owner full name' do
    assert @invoice.owner_full_name.present?
  end

  test 'customer full name' do
    assert @invoice.customer_full_name.present?
  end

  test 'owner_address' do
    assert @invoice.owner_address.present?
  end

  test 'customer address' do
    assert @invoice.customer_address.present?
  end

  test 'must be valid' do
    assert(@invoice.valid?)
  end

  test 'total for each user' do
    assert_equal [88], @invoice.total_for_user
  end

  test 'total for invoice' do

    assert_equal 88, @invoice.invoice_total(@invoice.total_for_user)
  end

end
