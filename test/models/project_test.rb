# frozen_string_literal: true

require 'test_helper'

class ProjectTest < ActiveSupport::TestCase


  test 'hour_by_day by project' do
    project = projects(:one)
    time = project.hours_by_user_by_project
    assert_nil time[1]
  end

  test 'hours by day' do
    project = Project.first
    time = project.hours_by_day
    assert_equal 1, time.count
  end



  def info_for_invoice_range(start_time, end_time)
    TimeSheet.where(time_period: start_time..end_time).joins(:works).group(:time_period).where('works.project_id = ?', id).sum(:hour)
  end

  test 'total timesheet query' do
    project = Project.first
    time = project.all_time_sheets
    time.each do |w|
      puts w.invoice_id.present?
      w.hour
    end
    assert_equal [88], project.total_for_users(time)
  end

  test 'total cost for timesheet search ' do
    total = 0
    time_array = []
    project = Project.first
    time = project.all_time_sheets
    time_array = project.total_for_users(time)
    total = project.total(time_array)
    assert_equal 88, total
  end


end
