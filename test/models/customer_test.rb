# frozen_string_literal: true

require 'test_helper'

class CustomerTest < ActiveSupport::TestCase

  def setup
    @customer = customers(:one)
  end

  test 'customer to be valid' do

    assert @customer.valid?
  end

  test 'customer full name' do
    assert @customer.full_name.present?
  end
end
