# frozen_string_literal: true

require 'test_helper'

class IdentityTest < ActiveSupport::TestCase
  test 'must be valid' do
    identity = identities(:one)
    identity.uid = "asdfasdfasdfdsfasdfasdf"
    assert(identity.valid?)
  end
end
