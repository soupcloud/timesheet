# frozen_string_literal: true

require 'test_helper'

class PayObigationTest < ActiveSupport::TestCase

  test 'is valid' do
    pay_obligations = pay_obligations(:one)
    assert(pay_obligations.valid?)
  end

  test 'calculate user charge rate' do

    pay_o = pay_obligations(:one)
    result = GrossPay.new(pay_o).calculate
    assert_equal(result,39.92004999)
  end
end
