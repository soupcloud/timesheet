# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should return pay rate' do
    user = User.first
    assert_equal 9, user.rate(Date.yesterday).to_i
  end

  test 'should be admin' do
    user = User.first
    user.add_role :admin
    result = user.has_role? :admin
    assert_equal true, result
  end


  # test 'user tax'
  #
  # test 'user super '
  #
  # test 'user total income'
  #
  # test 'pay user all projects '
  # test 'project select'
  # test 'pay per project' do
  #   user = User.first
  #   pararr = []
  #   time_sheet =	TimeSheet.joins(works: :project)
  #                         .select('works.hour', :project_id, :id, :user_id, :time_period)
  #                         .where(user_id: id)
  #
  #   time_sheet.each do |t|
  #     pay = t.hour * t.user.rate(t.time_period)
  #     pararr << { date: t.time_period, pay: pay, project: t.project_id }
  #   end
  #   pararr
  # end
end
