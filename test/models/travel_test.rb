# frozen_string_literal: true

require 'test_helper'

class TravelTest < ActiveSupport::TestCase
  test 'all travel' do
    travel = Travel.all
    assert_equal 2, travel.count
  end

  test 'travel for vehicle' do
    vehicle = vehicles(:one)
    assert vehicle.valid?
  end

  test 'odometer validation' do
    travel = travels(:one)
    assert travel.valid?
  end

  test 'od_start should be greater that previous od_finish' do
    travel = Travel.last
    assert_equal travel.previous_od_finish, 4
    assert_equal travel.od_finish ,4
  end
end
