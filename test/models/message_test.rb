# frozen_string_literal: true

require 'test_helper'


class MessageTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  def setup
    @message = messages(:one)
  end

  test 'must be valid' do
    assert(@message.valid?)
  end

  test 'timestamp' do
    assert @message.timestamp.present?
  end

  test 'broadcast messages' do
    assert_enqueued_jobs 0
    @message.send('send_message')
    assert_enqueued_jobs 1
  end
end
