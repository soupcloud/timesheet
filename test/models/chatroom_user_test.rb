# frozen_string_literal: true

require 'test_helper'

class ChatroomUserTest < ActiveSupport::TestCase
  test 'must be valid' do
    chatroom_user = chatroom_users(:one)
    assert(chatroom_user.valid?)
  end
end
