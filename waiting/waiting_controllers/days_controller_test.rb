require "test_helper"

describe DaysController do
  let(:day) { days :one }

  it "gets index" do
    get days_url
    value(response).must_be :success?
  end

  it "gets new" do
    get new_day_url
    value(response).must_be :success?
  end

  it "creates day" do
    expect {
      post days_url, params: { day: {  } }
    }.must_change "Day.count"

    must_redirect_to day_path(Day.last)
  end

  it "shows day" do
    get day_url(day)
    value(response).must_be :success?
  end

  it "gets edit" do
    get edit_day_url(day)
    value(response).must_be :success?
  end

  it "updates day" do
    patch day_url(day), params: { day: {  } }
    must_redirect_to day_path(day)
  end

  it "destroys day" do
    expect {
      delete day_url(day)
    }.must_change "Day.count", -1

    must_redirect_to days_path
  end
end
