class CreateStandardWorkdays < ActiveRecord::Migration[6.0]
  def change
    create_table :standard_workdays do |t|
      t.timestamp :start_time
      t.timestamp :finish_time
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
