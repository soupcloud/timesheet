class CreateFirstCompany < ActiveRecord::Migration[6.0]
  def up
      Company.reset_column_information
      Company.create!
  end

  def down
    Company.destroy_all
    ActiveRecord::Base.connection.reset_pk_sequence!('companies')
  end
end
