class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :abn
      t.string :slug
      t.string :name

      t.timestamps
    end
  end
  # class Company < ActiveRecord::Base; end
  #
  # Company.reset_column_information
  #
  # Company.create!
end
