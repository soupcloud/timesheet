class CreateInvoiceConfigurations < ActiveRecord::Migration[6.0]
  def change
    create_table :invoice_configurations do |t|
      t.references :company, foreign_key: true
      t.references :project, foreign_key: true
      t.integer :standard_period
      t.string :disclaimer
      t.timestamp :start_date

      t.timestamps
    end
  end
end
