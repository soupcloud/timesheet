class AddColumnsToInvoices < ActiveRecord::Migration[6.0]
  def change
    add_column :invoices, :disclaimer, :string
    add_column :invoices, :tax_rate, :float
    add_column :invoices, :tax_rate_label, :string
  end
end
