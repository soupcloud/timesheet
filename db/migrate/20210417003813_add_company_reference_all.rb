class AddCompanyReferenceAll < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :company, default: 1
    add_reference :chatroom_users, :company, default: 1
    add_reference :chatrooms, :company, default: 1
    add_reference :identities, :company, default: 1
    add_reference :messages, :company, default: 1
    add_reference :pay_rates, :company, default: 1
    add_reference :projects, :company, default: 1
    add_reference :roles, :company, default: 1
    add_reference :time_sheets, :company, default: 1
    add_reference :travels, :company, default: 1
    add_reference :user_pay_obligations, :company, default: 1
    add_reference :users_roles, :company, default: 1
    add_reference :vehicles, :company, default: 1
    add_reference :works, :company, default: 1
    add_reference :invoices, :company, default: 1
    add_reference :customers, :company, default: 1

  end
end
