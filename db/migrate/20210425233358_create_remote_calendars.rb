class CreateRemoteCalendars < ActiveRecord::Migration[6.0]
  def change
    create_table :remote_calendars do |t|
      t.string :name
      t.references :identity, foreign_key: true
      t.references :company, foreign_key: true
      t.string :remote_system_id

      t.timestamps
    end
  end
end
