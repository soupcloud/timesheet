class CreateEventImporters < ActiveRecord::Migration[6.0]
  def change
    create_table :event_importers do |t|
      t.json :data, default: {}
      t.string :email
      t.string :description
      t.timestamp :start
      t.timestamp :finish
      t.string :date
      t.references :project, foreign_key: true
      t.references :company, foreign_key: true
      t.references :user, foreign_key: true
      t.references :time_sheet, foreign_key: true

      t.timestamps
    end
  end
end
