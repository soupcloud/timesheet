class AddAddressToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :street_no, :string
    add_column :companies, :street, :string
    add_column :companies, :state, :string
    add_column :companies, :country, :string
    add_column :companies, :post_code, :string
    add_column :companies, :suburb, :string
    add_column :companies, :email, :string
  end
end
