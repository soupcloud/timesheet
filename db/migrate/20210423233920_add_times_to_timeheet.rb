class AddTimesToTimeheet < ActiveRecord::Migration[6.0]
  def change
    add_column :time_sheets, :start_time, :datetime
    add_column :time_sheets, :finish_time, :datetime
  end
end
