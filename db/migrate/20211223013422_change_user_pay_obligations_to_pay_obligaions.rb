class ChangeUserPayObligationsToPayObligaions < ActiveRecord::Migration[6.0]
  def change
    rename_table :user_pay_obligations, :pay_obligations
  end
end
