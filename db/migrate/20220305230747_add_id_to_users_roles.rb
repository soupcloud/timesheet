class AddIdToUsersRoles < ActiveRecord::Migration[6.0]
  def change
    add_column :users_roles, :id, :primary_key
  end
end
