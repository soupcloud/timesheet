class AddExpirestAtToIdentity < ActiveRecord::Migration[6.0]
  def change
    add_column :identities, :expires_at, :datetime
    add_column :identities, :refresh_token, :string
  end
end
