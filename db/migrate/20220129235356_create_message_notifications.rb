class CreateMessageNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :message_notifications do |t|
      t.references :chatroom, foreign_key: true
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true
      t.timestamp :read_at
      t.references :message, foreign_key: true

      t.timestamps
    end
  end
end
