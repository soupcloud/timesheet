class AddUuidToUsers < ActiveRecord::Migration[6.0]
  def change
    enable_extension 'uuid-ossp'
    remove_column :users, :authentication_token
    add_column :users, :authentication_token, :uuid, default: "uuid_generate_v4()", null: false

  end
end
