class CreateWeekHours < ActiveRecord::Migration[6.0]
  def change
    create_table :week_hours do |t|
      t.integer :week
      t.integer :year
      t.float :hours
      t.references :user, foreign_key: true
      t.references :company, foreign_key: true
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
