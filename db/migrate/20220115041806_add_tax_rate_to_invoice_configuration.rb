class AddTaxRateToInvoiceConfiguration < ActiveRecord::Migration[6.0]
  def change
    add_column :invoice_configurations, :tax_rate, :float
    add_column :invoice_configurations, :tax_rate_label, :string
  end
end
