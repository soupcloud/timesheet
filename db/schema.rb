# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_29_235356) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "chatroom_users", force: :cascade do |t|
    t.bigint "chatroom_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_read_at"
    t.bigint "company_id", default: 1
    t.index ["chatroom_id"], name: "index_chatroom_users_on_chatroom_id"
    t.index ["company_id"], name: "index_chatroom_users_on_company_id"
    t.index ["user_id"], name: "index_chatroom_users_on_user_id"
  end

  create_table "chatrooms", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_chatrooms_on_company_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "abn"
    t.string "slug"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "street_no"
    t.string "street"
    t.string "state"
    t.string "country"
    t.string "post_code"
    t.string "suburb"
    t.string "email"
    t.string "city"
  end

  create_table "customers", id: :serial, force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "street_no"
    t.string "street"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "post_code"
    t.string "abn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "email"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_customers_on_company_id"
    t.index ["user_id"], name: "index_customers_on_user_id"
  end

  create_table "descriptions", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "event_importers", force: :cascade do |t|
    t.json "data", default: {}
    t.string "email"
    t.string "description"
    t.datetime "start"
    t.datetime "finish"
    t.string "date"
    t.bigint "project_id"
    t.bigint "company_id"
    t.bigint "user_id"
    t.bigint "time_sheet_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_event_importers_on_company_id"
    t.index ["project_id"], name: "index_event_importers_on_project_id"
    t.index ["time_sheet_id"], name: "index_event_importers_on_time_sheet_id"
    t.index ["user_id"], name: "index_event_importers_on_user_id"
  end

  create_table "identities", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "uid"
    t.string "provider"
    t.string "token"
    t.string "secrect"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "profile_page"
    t.string "image"
    t.string "username"
    t.string "email"
    t.bigint "company_id", default: 1
    t.datetime "expires_at"
    t.string "refresh_token"
    t.index ["company_id"], name: "index_identities_on_company_id"
  end

  create_table "invoice_configurations", force: :cascade do |t|
    t.bigint "company_id"
    t.bigint "project_id"
    t.integer "standard_period"
    t.string "disclaimer"
    t.datetime "start_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "tax_rate"
    t.string "tax_rate_label"
    t.index ["company_id"], name: "index_invoice_configurations_on_company_id"
    t.index ["project_id"], name: "index_invoice_configurations_on_project_id"
  end

  create_table "invoices", id: :serial, force: :cascade do |t|
    t.string "name"
    t.date "endOfFortnight"
    t.integer "user_id"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "customer_id"
    t.string "owner_first_name"
    t.string "owner_last_name"
    t.string "owner_email"
    t.string "owner_street_no"
    t.string "owner_street"
    t.string "owner_city"
    t.string "owner_country"
    t.string "owner_post_code"
    t.string "owner_abn"
    t.string "customer_first_name"
    t.string "customer_last_name"
    t.string "customer_email"
    t.string "customer_street_no"
    t.string "customer_street"
    t.string "customer_city"
    t.string "customer_country"
    t.string "customer_post_code"
    t.string "customer_abn"
    t.date "invoice_date"
    t.string "customer_state"
    t.string "owner_state"
    t.bigint "company_id", default: 1
    t.string "disclaimer"
    t.float "tax_rate"
    t.string "tax_rate_label"
    t.index ["company_id"], name: "index_invoices_on_company_id"
    t.index ["customer_id"], name: "index_invoices_on_customer_id"
    t.index ["project_id"], name: "index_invoices_on_project_id"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "loggintracker_public_ar_internal_metadata", id: false, force: :cascade do |t|
    t.text "key"
    t.text "value"
    t.text "created_at"
    t.text "updated_at"
  end

  create_table "message_notifications", force: :cascade do |t|
    t.bigint "chatroom_id"
    t.bigint "user_id"
    t.bigint "company_id"
    t.datetime "read_at"
    t.bigint "message_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["chatroom_id"], name: "index_message_notifications_on_chatroom_id"
    t.index ["company_id"], name: "index_message_notifications_on_company_id"
    t.index ["message_id"], name: "index_message_notifications_on_message_id"
    t.index ["user_id"], name: "index_message_notifications_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "chatroom_id"
    t.bigint "company_id", default: 1
    t.index ["chatroom_id"], name: "index_messages_on_chatroom_id"
    t.index ["company_id"], name: "index_messages_on_company_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "pay_obligations", force: :cascade do |t|
    t.decimal "superannuation", precision: 5, scale: 2
    t.decimal "holiday", precision: 5, scale: 2
    t.decimal "hourly_rate", precision: 5, scale: 2
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_pay_obligations_on_company_id"
    t.index ["user_id"], name: "index_pay_obligations_on_user_id"
  end

  create_table "pay_rates", id: :serial, force: :cascade do |t|
    t.decimal "rate"
    t.integer "project_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "holiday"
    t.decimal "superannuation"
    t.datetime "date"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_pay_rates_on_company_id"
    t.index ["project_id"], name: "index_pay_rates_on_project_id"
    t.index ["user_id"], name: "index_pay_rates_on_user_id"
  end

  create_table "projects", id: :serial, force: :cascade do |t|
    t.string "name"
    t.float "hours"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "customer_id"
    t.integer "user_id"
    t.string "gitname"
    t.string "branch"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_projects_on_company_id"
    t.index ["customer_id"], name: "index_projects_on_customer_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "remote_calendars", force: :cascade do |t|
    t.string "name"
    t.bigint "identity_id"
    t.bigint "company_id"
    t.string "remote_system_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_remote_calendars_on_company_id"
    t.index ["identity_id"], name: "index_remote_calendars_on_identity_id"
  end

  create_table "roles", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.integer "resource_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_roles_on_company_id"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
  end

  create_table "standard_workdays", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "finish_time"
    t.bigint "user_id"
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_standard_workdays_on_company_id"
    t.index ["user_id"], name: "index_standard_workdays_on_user_id"
  end

  create_table "time_sheets", id: :serial, force: :cascade do |t|
    t.date "time_period"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "hour"
    t.string "description"
    t.datetime "date"
    t.bigint "project_id"
    t.bigint "invoice_id"
    t.bigint "company_id", default: 1
    t.datetime "start_time"
    t.datetime "finish_time"
    t.string "type"
    t.index ["company_id"], name: "index_time_sheets_on_company_id"
    t.index ["invoice_id"], name: "index_time_sheets_on_invoices_id"
    t.index ["project_id"], name: "index_time_sheets_on_projects_id"
    t.index ["user_id"], name: "index_time_sheets_on_user_id"
  end

  create_table "travels", force: :cascade do |t|
    t.integer "od_start"
    t.integer "od_finish"
    t.string "purpose"
    t.bigint "user_id"
    t.bigint "project_id"
    t.bigint "time_sheet_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "vehicle_id"
    t.date "travel_date"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_travels_on_company_id"
    t.index ["project_id"], name: "index_travels_on_project_id"
    t.index ["time_sheet_id"], name: "index_travels_on_time_sheet_id"
    t.index ["user_id"], name: "index_travels_on_user_id"
    t.index ["vehicle_id"], name: "index_travels_on_vehicle_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "uid"
    t.string "username"
    t.string "oauth_token"
    t.datetime "oauth_expires_at"
    t.string "first_name"
    t.string "last_name"
    t.string "street_no"
    t.string "street"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "post_code"
    t.string "abn"
    t.uuid "authentication_token", default: -> { "uuid_generate_v4()" }, null: false
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_users_on_company_id"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_users_roles_on_company_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "rego_no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_vehicles_on_company_id"
  end

  create_table "week_hours", force: :cascade do |t|
    t.integer "week"
    t.integer "year"
    t.float "hours"
    t.bigint "user_id"
    t.bigint "company_id"
    t.bigint "project_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_week_hours_on_company_id"
    t.index ["project_id"], name: "index_week_hours_on_project_id"
    t.index ["user_id"], name: "index_week_hours_on_user_id"
  end

  create_table "works", id: :serial, force: :cascade do |t|
    t.date "date"
    t.float "hour"
    t.string "description"
    t.integer "project_id"
    t.integer "time_sheet_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "invoice_id"
    t.bigint "company_id", default: 1
    t.index ["company_id"], name: "index_works_on_company_id"
    t.index ["invoice_id"], name: "index_works_on_invoice_id"
    t.index ["project_id"], name: "index_works_on_project_id"
    t.index ["time_sheet_id"], name: "index_works_on_time_sheet_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "chatroom_users", "chatrooms"
  add_foreign_key "chatroom_users", "users"
  add_foreign_key "customers", "users"
  add_foreign_key "event_importers", "companies"
  add_foreign_key "event_importers", "projects"
  add_foreign_key "event_importers", "time_sheets"
  add_foreign_key "event_importers", "users"
  add_foreign_key "invoice_configurations", "companies"
  add_foreign_key "invoice_configurations", "projects"
  add_foreign_key "invoices", "customers"
  add_foreign_key "message_notifications", "chatrooms"
  add_foreign_key "message_notifications", "companies"
  add_foreign_key "message_notifications", "messages"
  add_foreign_key "message_notifications", "users"
  add_foreign_key "messages", "chatrooms"
  add_foreign_key "messages", "users"
  add_foreign_key "pay_obligations", "users"
  add_foreign_key "pay_rates", "projects"
  add_foreign_key "pay_rates", "users"
  add_foreign_key "projects", "customers"
  add_foreign_key "projects", "users"
  add_foreign_key "remote_calendars", "companies"
  add_foreign_key "remote_calendars", "identities"
  add_foreign_key "standard_workdays", "companies"
  add_foreign_key "standard_workdays", "users"
  add_foreign_key "time_sheets", "invoices"
  add_foreign_key "time_sheets", "projects"
  add_foreign_key "time_sheets", "users"
  add_foreign_key "travels", "projects"
  add_foreign_key "travels", "time_sheets"
  add_foreign_key "travels", "users"
  add_foreign_key "travels", "vehicles"
  add_foreign_key "week_hours", "companies"
  add_foreign_key "week_hours", "projects"
  add_foreign_key "week_hours", "users"
  add_foreign_key "works", "invoices"
  add_foreign_key "works", "projects"
  add_foreign_key "works", "time_sheets"
end
