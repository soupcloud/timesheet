class GoogleCalendar
  require "google/apis/calendar_v3"
  attr_accessor :auth, :g_calendar

  def initialize(options = {})
    # @day = options[:day] if options[:day].present?
    @auth = options[:token] if options[:token].present?
    client = Signet::OAuth2::Client.new(access_token: auth)
    @g_calendar = Google::Apis::CalendarV3::CalendarService.new
    @g_calendar.authorization = client
  end

  def list_calendars
    labels_list = g_calendar.list_calendar_lists
    labels_list.items.map { |c| [c.id, c.summary] }
  end

  def list_events(calendar_id, time_min, time_max)
    labels_list = g_calendar.list_events(calendar_id, {time_min: time_min, time_max: time_max})
    labels_list.items.map { |c| [c.id, c.summary] }
  end

  def list_event_instances(calendar_id, time_min, time_max, timezone)
    events = g_calendar.list_events(calendar_id, {time_min: time_min, time_max: time_max, time_zone: timezone})
    # labels_list.items.map { |c| [c.id, c.summary] }
    levents  =  events.items.map do |m|
      begin
      {email: m.creator.email.to_s, description: m.summary.to_s, start: m.start.date_time, finish: m.end.date_time, date: parse_event_date(m.start)}
      rescue
      end
    end
    levents
  end

  def full_list_event_instances(calendar_id, time_min, time_max, timezone)
    g_calendar.list_events(calendar_id, {time_min: time_min, time_max: time_max, time_zone: timezone})
  end

  def parse_event_date(start)
    date = start.date_time.present? ? start.date_time : start.date
    date&.strftime("%Y-%m-%d")
  end

  def get_event(calendar_id, event_id)
    g_calendar.get_event(calendar_id, event_id)
  end

  def update_or_create_event(calendar_id: nil, attendee: nil, summary: nil, location: nil, description: nil, start_time: nil, end_time: nil, timezone: nil)
    event = set_event({calendar_id: calendar_id, attendee: attendee, summary: summary, location: location,
                       description: description, start_time: start_time, end_time: end_time, timezone: timezone})
    old_events = g_calendar.list_events(calendar_id, {time_min: start_time, time_max: end_time})
    old_event = old_events.items.select { |event| event if event.summary == summary }&.first
    if old_event.present?
      g_calendar.update_event(calendar_id, old_event.id, event)
    else
      g_calendar.insert_event(calendar_id, event)
    end
  end

  def insert_event(calendar_id: nil, attendee: nil, summary: nil, location: nil, description: nil, start_time: nil, end_time: nil, timezone: nil)
    event = set_event({calendar_id: calendar_id, attendee: attendee, summary: summary, location: location,
                       description: description, start_time: start_time, end_time: end_time, timezone: timezone})
    @g_calendar.insert_event(calendar_id, event)
  end

  private

  def set_event(calendar_id: nil, attendee: nil, summary: nil, location: nil, description: nil, start_time: nil, end_time: nil, timezone: nil)
    Google::Apis::CalendarV3::Event.new(
      summary: summary,
      location: location,
      description: description,
      sendUpdates: "none",
      start: Google::Apis::CalendarV3::EventDateTime.new(
        date_time: start_time,
        time_zone: timezone
      ),
      end: Google::Apis::CalendarV3::EventDateTime.new(
        date_time: end_time,
        time_zone: timezone
      )
      # recurrence: [
      #   'RRULE:FREQ=DAILY;COUNT=2'
      # ],
      # attendees: [
      #   Google::Apis::CalendarV3::EventAttendee.new(
      #     email: attendee
      #   ),
      # ],
      # reminders: Google::Apis::CalendarV3::Event::Reminders.new(
      #   use_default: false,
      #   overrides: [
      #     Google::Apis::CalendarV3::EventReminder.new(
      #       reminder_method: 'email',
      #       minutes: 24 * 60
      #     ),
      #     Google::Apis::CalendarV3::EventReminder.new(
      #       reminder_method: 'popup',
      #       minutes: 10
      #     )
      #   ]
      # )
    )
  end
end

#     private
#
#     def get_list(url)
#       http = Net::HTTP.new(url.host, url.port)
#       http.use_ssl = true
#       req = Net::HTTP::Get.new(url.request_uri)
#       req["Accept"] = 'application/vnd.github.cloak-preview'
#       req["Authorization"] = "token #{@auth}" if @auth.present?
#       res = http.request(req)
#       JSON.parse(res.body)
#     end
#   end
# #https://api.github.com/repos/ChrisMcdonald/TimeSheet/commits?committer=ChrisMcdonald
