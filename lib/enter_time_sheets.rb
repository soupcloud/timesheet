class EnterTimeSheets
  attr_accessor :event

  def initialize(event)
    @event = event
  end

  def process
    # events.each do |event|
    user = User.find_by(email: event.email, company: event.company)
    timesheet = if event.start.present?
      TimeSheet::WorkDay.find_or_initialize_by({user: user, time_period: event.date})
    else
      TimeSheet::Rdo.find_or_initialize_by({user: user, time_period: event.date})
    end
    set_attrs(timesheet, event)
    # end
    event
  end

  def set_attrs(timesheet, event)
    timesheet.time_period = event.date
    timesheet.description = event.description
    timesheet.start_time = event.start
    timesheet.finish_time = event.finish
    timesheet.project_id = event.project_id
    timesheet.set_hours
    timesheet.save
    event.time_sheet = timesheet
  end
end
