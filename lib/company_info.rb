module CompanyInfo
  def current_company
    Thread.current[:company_id]
  end

  def self.current_company=(company_id)
    Thread.current[:company_id] = company_id
  end
end
