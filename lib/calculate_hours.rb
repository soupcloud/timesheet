class CalculateHours
  attr_accessor :date ,:user, :project

  def initialize(user: nil, project: nil)
    Thread.current[:company_id] = 52
    # @date = Date.current - 21.weeks
    @user = user ||  User.find(16)
    @project = project || Project.find(19)
  end

  def for_week(date)

    week_number = date.strftime('%U')
    year = date.strftime("%Y")
    beginning = date.beginning_of_week
    finish = date.end_of_week
    hours = TimeSheet.where(user: user, project: project,time_period: beginning..finish).sum(:hour)
    WeekHour.create(week: week_number,year: year, user: user, project: project, hours: hours )
  end



end
