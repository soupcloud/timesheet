# frozen_string_literal: true

source 'http://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end
gem 'rails', '6.0.0'
ruby '2.7.2'
gem 'rack'
gem 'figaro'
gem 'active_median'
gem 'autoprefixer-rails'
# gem 'axlsx'
gem 'caxlsx'
gem 'caxlsx_rails'
gem 'rollbar'
gem 'bootstrap-sass', '~> 3.4.1'
gem 'sassc-rails', '>= 2.1.0'
gem 'breadcrumbs_on_rails'
gem 'cancancan'
gem 'carrierwave'
gem 'chartkick'
gem 'devise'
gem 'font-awesome-rails'
gem 'awesome_print'
gem 'groupdate'
gem 'loofah'
gem 'nokogiri'
gem 'oj'
gem 'omniauth',"~> 1.9.1"
# gem 'omniauth-facebook'
# gem 'omniauth-github'
gem 'omniauth-google-oauth2'
gem 'google-api-client', '~> 0.53.0'
# gem 'omniauth-twitter'
gem 'pdfkit'
gem 'pundit'
gem 'remotipart'
gem 'rolify'
gem 'rubyzip', '~> 2.0'
gem 'sass-rails'
gem 'simple_calendar'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'zip-zip'
gem 'webpacker'
gem "simple_token_authentication"
gem 'react-rails'
gem 'devise_token_auth'
gem 'active_model_serializers', '~> 0.10.2'
gem 'jwt'
gem 'mini_magick'


# gem "uniform_notifier"
# gem 'honeybadger'
# gem 'airbrake'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
# Use postgresql as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma'
# Use SCSS for stylesheets
gem 'momentjs-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'libv8',  '~> 8.4.255.0' #--source 'http://rubygems.org/'

gem 'mini_racer', platforms: :ruby
# gem 'therubyracer', platforms: :ruby
gem 'will_paginate'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.3'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'loofah-activerecord'
group :development, :test do
  gem 'pry'
  gem 'pry-nav'
  gem 'rubocop'
  gem 'mina'
end

group :development do
  gem 'yard'
  gem 'listen'
  gem 'web-console'
  gem 'spring'
  gem 'spring-watcher-listen'
end

group :test do
  # gem "minitest-rails-capybara"
  gem 'simplecov', require: false
  gem 'webdrivers'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'faker'
  gem 'selenium-webdriver'
  # gem 'sqlite3'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
